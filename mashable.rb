class Mashable 
	
	def  get_data 

		url =  'https://mashable.com/stories.json'
		response = RestClient.get (url)
		@resphash = JSON.parse(response)

	end
	def sort_data
		@resphash['new'].sort_by! {|x| x['post_date_rfc']}
		@resphash['rising'].sort_by! {|x| x['post_date_rfc']}
		@resphash['hot'].sort_by! {|x| x['post_date_rfc']}
	end

	def put_data_new
		system('clear')
			# puts "<<<<<<<<<<<<<<<NEW>>>>>>>>>>>>>>>>>>".colorize(:red)
		@resphash['new'].each do |x| 	
			puts "Titulo: #{x['title']}".colorize(:green)
			puts "Autor: #{x['author']}"
			puts "Enlace:" +" #{x['link']}".colorize(:light_blue)
			puts Time.parse(x['post_date_rfc']).getlocal.strftime("Creado el %d/%m/%Y a las %H:%M".colorize(:yellow))
			puts "-----------------------------------------------"
		end
	end

	# def put_data_rising
	# 		puts "<<<<<<<<<<<<<<<RISING>>>>>>>>>>>>>>>>".colorize(:red)
	# 	@resphash['rising'].each do |x| 	
	# 		puts "Titulo: #{x['title']}".colorize(:green)
	# 		puts "Autor: #{x['author']}"
	# 		puts "Enlace:" +" #{x['link']}".colorize(:light_blue)
	# 		puts Time.parse(x['post_date_rfc']).getlocal.strftime("Creado el %d/%m/%Y a las %H:%M".colorize(:yellow))
	# 		puts "-----------------------------------------------"
	# 	end
	# end
	# def put_data_hot
	# 		puts "<<<<<<<<<<<<<<<HOT>>>>>>>>>>>>>>>>".colorize(:red)
	# 	@resphash['hot'].each do |x| 	
	# 		puts "Titulo: #{x['title']}".colorize(:green)
	# 		puts "Autor: #{x['author']}"
	# 		puts "Enlace:" +" #{x['link']}".colorize(:light_blue)
	# 		puts Time.parse(x['post_date_rfc']).getlocal.strftime("Creado el %d/%m/%Y a las %H:%M".colorize(:yellow))
	# 		puts "-----------------------------------------------"
	# 	end
	# end



end

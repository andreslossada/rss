class Digg

	def get_data 

		url =  'http://digg.com/api/news/popular.json'
		response = RestClient.get(url)
		@resphash = JSON.parse(response)

	end

	def sort_data
		@resphash['data']['feed'].sort_by! {|x| x['date']}
	end

	def put_data
		system('clear')
		@resphash['data']['feed'].each do |x|
			puts "Titulo: #{x['content']['title_alt']}".colorize(:green)
				if x['content']['author'] == ""
					puts "Autor: Anonimo"
				else
				puts "Autor: #{x['content']['author']}"
				end	
			puts "Link:"+" #{x['content']['url']}".colorize(:light_blue)
			puts Time.at(x['date']).strftime("Creado el %d/%m/%Y a las %H:%M".colorize(:yellow)) 
			puts "-----------------------------------------------"
		end
	end

end

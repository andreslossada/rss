require 'json'
require 'rest-client'
require 'colorize'
require_relative './redditnew.rb'
require_relative './digg.rb'
require_relative './mashable.rb'

def get_keypressed
	system("stty raw -echo")
	@key = STDIN.getc
	system("stty -raw echo")
	return @key
end

def load
	loop do
		loop do
			system('clear')
			puts "Cual pagina desea acceder? \n 1) Reddit \n 2) Mashable \n 3) Digg "
			@key = get_keypressed.downcase.to_i
		break if @key==1 || @key == 2 || @key == 3
		end

		system('clear')
		puts "Cargando informacion...".blink
			case @key
				when 1
					n1 = Reddit.new
					n1.get_data
					n1.sort_data
					n1.put_data
				when 2
					n2 = Mashable.new
					n2.get_data
					n2.sort_data
					n2.put_data_new

				when 3
					n3 = Digg.new
					n3.get_data
					n3.sort_data
					n3.put_data
		
			end

	puts "Presione S para salir o !S para volver"
	@key = get_keypressed.downcase
	break if @key == 's'
	end
end
load
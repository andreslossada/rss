class Reddit

	def get_data 

		url =  'https://www.reddit.com/.json'
		response = RestClient.get(url)
		@resphash = JSON.parse(response)

	end

	def sort_data
		@resphash['data']['children'].sort_by! {|y| y['data']['created_utc']} 
	end

	def put_data
		system('clear')
		
		@resphash['data']['children'].each do |x|
			puts "Titulo: #{x['data']['title']}".colorize(:green)
			puts "Nickname del autor: #{x['data']['author']}"
			puts "Link:"+" https://www.reddit.com/#{x['data']['permalink']}".colorize(:blue)
			puts Time.at(x['data']['created_utc']).strftime("Creado el %d/%m/%Y a las %H:%M".colorize(:yellow)) 
			puts "-----------------------------------------------"
		end
	
	end

end
